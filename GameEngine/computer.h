#ifndef COMPUTER_H
#define COMPUTER_H

#include "player.h"

class Computer : Player
{
public:
    Computer();
    Computer(std::string name);
};

#endif // COMPUTER_H
