#ifndef HUMAN_H
#define HUMAN_H

#include "player.h"

class Human : Player
{
public:
    Human();
    Human(std::string name);
};

#endif // HUMAN_H
