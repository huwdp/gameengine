#include "coal.h"

Coal::Coal(float x, float y, float z) : Drawable(x, y, z)
{

}

void Coal::draw()
{
    glBegin(GL_POLYGON);

    glColor3b(1.0, 1.0, 1.0);
    glVertex3f( -0.5, -0.5, -0.5);       // P1
    glVertex3f( -0.5,  0.5, -0.5);       // P2
    glVertex3f(  0.5,  0.5, -0.5);       // P3
    glVertex3f(  0.5, -0.5, -0.5);       // P4

    glEnd();
}
